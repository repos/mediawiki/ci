# MediaWiki CI configuration for GitLab

This repository contains shared GitLab CI configuration that can be included
by individual MediaWiki repos for a standard set of CI jobs.
